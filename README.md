# dotfiles

## GTK configuration

### Startup Mode

#### GTK 3
`gsettings set org.gtk.Settings.FileChooser startup-mode cwd`

#### GTK 2
`echo 'StartupMode=cwd' >> $HOME/.config/gtk-2.0/gtkfilechooser.ini`

### Theme

#### GTK 3,4
`gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"`

# GTK 2
`mkdir ~/.themes && ln -s /usr/share/themes/Adwaita-dark ~/.themes/Adwaita`

### Icons

`gsettings set org.gnome.desktop.interface icon-theme 'Newaita'`

Default: 'Gnome'

### Sound Theme

Directory locations: 

#### System Wide
`/usr/share/sounds`

#### Per user:
`~/.local/share/sounds`

`gsettings set org.gnome.desktop.sound theme-name 'freedesktop'`

Default: 'freedesktop'

`gsettings set org.gnome.desktop.sound event-sounds true`

Default: true

`gsettings set org.gnome.desktop.sound input-feedback-sounds true`

Default: false


#### File Chooser

Sort directories first
`gsettings set org.gtk.Settings.FileChooser sort-directories-first true`