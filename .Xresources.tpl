#include ".xresources.d/catppuccin/macchiato.Xresources"

!! STRING,  &font },
!st.font: "UbuntuMono Nerd Font Mono:pixelsize=13:antialias=true:autohint=true"
! st.color0: black  !! STRING,  &colorname[0] },
! st.color1: red  !! STRING,  &colorname[1] },
! st.color2: green  !! STRING,  &colorname[2] },
! st.color3: yellow  !! STRING,  &colorname[3] },
! st.color4: blue  !! STRING,  &colorname[4] },
! st.color5: magenta  !! STRING,  &colorname[5] },
! st.color6: cyan  !! STRING,  &colorname[6] },
! st.color7: white  !! STRING,  &colorname[7] },
! st.color8: black2  !! STRING,  &colorname[8] },
! st.color9: red2  !! STRING,  &colorname[9] },
! st.color10: green2  !! STRING,  &colorname[10] },
! st.color11: yellow2  !! STRING,  &colorname[11] },
! st.color12: blue2  !! STRING,  &colorname[12] },
! st.color13: magenta2  !! STRING,  &colorname[13] },
! st.color14: cyan2  !! STRING,  &colorname[14] },
! st.color15: white2  !! STRING,  &colorname[15] },
!! STRING,  &colorname[256] },
!st.background: BACKGROUND
! st.foreground: foreground  !! STRING,  &colorname[257] },
! st.cursorColor:  !! STRING,  &colorname[258] },
! st.termname:  !! STRING,  &termname },
! st.shell:  !! STRING,  &shell },
! st.minlatency:  !! INTEGER, &minlatency },
! st.maxlatency:  !! INTEGER, &maxlatency },
! st.blinktimeout:  !! INTEGER, &blinktimeout },
! st.bellvolume:  !! INTEGER, &bellvolume },
! st.tabspaces:  !! INTEGER, &tabspaces },
! st.borderpx:  !! INTEGER, &borderpx },
! st.cwscale:  !! FLOAT,   &cwscale },
! st.chscale:  !! FLOAT,   &chscale },

dwm.normbordercolor:   BORDER
dwm.normbgcolor:       BACKGROUND
dwm.normfgcolor:       FOREGROUND
dwm.selbordercolor:    SELBORDER
dwm.selbgcolor:        BACKGROUND
dwm.selfgcolor:        BLUE 

!dmenu.prompt:
dmenu.font:             Ubuntu\ Mono:pixelsize=25:antialias=true:autohint=true:hintstyle=hintfull
dmenu.normfgcolor:      FOREGROUND
dmenu.normbgcolor:      BACKGROUND
dmenu.selfgcolor:       BLUE
dmenu.selbgcolor:       BACKGROUND

*background:            #1E1E2E
*foreground:            #CDD6F4

! black
*color0:                #45475A
*color8:                #585B70

! red
*color1:                #F38BA8
*color9:                #F38BA8

! green
*color2:                #A6E3A1
*color10:               #A6E3A1

! yellow
*color3:                #F9E2AF
*color11:               #F9E2AF

! blue
*color4:                #89B4FA
*color12:               #89B4FA

! magenta
*color5:                #F5C2E7
*color13:               #F5C2E7

! cyan
*color6:                #94E2D5
*color14:               #94E2D5

! white
*color7:                #BAC2DE
*color15:               #A6ADC8
