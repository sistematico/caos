vim.opt.tabstop = 2 -- Always 8 (see :h tabstop)
vim.opt.softtabstop = 2 -- What you expecting
vim.opt.shiftwidth = 2 -- What you expecting
vim.g.mapleader = ' '
vim.g.sonokai_style = 'espresso'
-- let g:sonokai_better_performance = 1
