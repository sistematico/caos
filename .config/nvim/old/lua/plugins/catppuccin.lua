return {
  'catppuccin/nvim',
  name = 'catppuccin',
  priority = 1000,
  transparent_background = true,
  config = function()
    vim.cmd.colorscheme 'catppuccin-mocha'
    require("catppuccin").setup({
      transparent_background = true
    })
  end
}
