#!/usr/bin/env bash

[ ! $1 ] && exit

if curl --head --silent "$1" > /dev/null 2>&1; then
    $BROWSER "$1" &
fi

[ -d "$1" ] && thunar "$1" &
[ -f "$1" ] && leafpad "$1" &

