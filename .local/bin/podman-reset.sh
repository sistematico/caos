#!/bin/bash

# Parar todos os contêineres
echo "Parando todos os contêineres..."
podman stop $(podman ps -aq) 2>/dev/null

# Parando todos os pods
echo "Parando todos os pods..."
podman pod stop --all

# Removendo todos os pods
echo "Removendo todos os pods..."
podman pod rm --all

# Remover todos os contêineres
echo "Removendo todos os contêineres..."
podman rm $(podman ps -aq) 2>/dev/null

# Remover todas as imagens
echo "Removendo todas as imagens..."
podman rmi $(podman images -q) 2>/dev/null

# Remover todos os volumes
echo "Removendo todos os volumes..."
podman volume rm $(podman volume ls -q) 2>/dev/null

# Remover todas as redes (em versões do Podman que suportam gerenciamento de rede)
if podman network ls &>/dev/null; then
    echo "Removendo todas as redes..."
    podman network rm $(podman network ls -q) 2>/dev/null
fi

sudo podman system reset --force

echo "Tudo foi resetado no Podman."
