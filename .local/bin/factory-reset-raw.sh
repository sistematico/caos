#!/usr/bin/env bash
#
# Arquivo: factory-reset-raw.sh
#
# Feito por Lucas Saliés Brum a.k.a. Paxá, <paxa@paxa.dev>
#
# Criado em: 16/03/2018 16:35:20
# Última alteração: 08/02/2024 12:32:35

AMD="amd-ucode xf86-video-amdgpu"
XORG="xorg-server xorg-xinit xclip libxft xorg-xrandr"
XFCE="xfce4 networkmanager network-manager-applet lightdm lightdm-gtk-greeter"
BASE="base base-devel linux linux-headers linux-firmware efibootmgr lvm2 grub os-prober ntfs-3g dhcpcd zsh links curl sudo nano git rsync openssh pulseaudio"
TOPT="feh dunst picom-ibhagwan-git maim dracula-gtk-theme"
OPT="eza trash-cli gnome-themes-extra newaita-icons-git starship fortune-mod fortune-mod-chucknorris pavucontrol"
FONTS="ttf-jetbrains-mono-nerd ttf-ubuntu-font-family ttf-fantasque-sans-mono"

PKGS="${BASE} ${AMD} ${OPT} ${FONTS}"

[ ! -f /etc/systemd/network/20-wired.network ] && curl -L -s -o /etc/systemd/network/20-wired.network https://termbin.com/v15n
[ ! -f /etc/systemd/network/loopback-alias.network ] && curl -L -s -o /etc/systemd/network/10-loopback-alias.network https://termbin.com/g4k7
[ ! -L /etc/resolv.conf ] && rm -f /etc/resolv.conf && ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf

systemctl --now disable docker podman
systemctl disable gdm lightdm NetworkManager iwd
systemctl enable systemd-networkd systemd-resolved

echo "Packages: $(pacman -Q | wc -l)" > /var/tmp/packages-before.log
echo "---" >> /var/tmp/packages-before.log
pacman -Q >> /var/tmp/packages-before.log

# Mark all as optional
pacman -D --asdeps $(pacman -Qqe)

# Mark base packages as explicit
pacman -D --asexplicit $PKGS

# Remove all except explicit packages
# Note: The arguments -Qt list only true orphans.
# To include packages which are optionally required by another package, pass the -t flag twice (i.e., -Qtt).
pacman -Rns $(pacman -Qttdq)

# Update all packages
pacman -Syyu
