#!/usr/bin/env bash

sensors | awk '/Tctl/{print $2;exit}' | awk '{print substr($0, 2, length($0) - 3)}'
