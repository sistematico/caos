# Vars used in functions.
EDITOR="nvim"
STORAGE="$HOME"

# aur
function pkgup() {
  [ ! $1 ] && return
  makepkg --printsrcinfo > .SRCINFO
  git add PKGBUILD .SRCINFO $@
  git commit -m "version bumped"
  git push
}

# utils
function ndays() {
	# Verifica se um argumento foi fornecido
	if [ $# -ne 1 ]; then
    	echo "Uso: $0 <numero_de_dias>"
    	exit 1
	fi

	# O argumento deve ser um número inteiro
	if ! [[ $1 =~ ^-?[0-9]+$ ]]; then
    	echo "Erro: O argumento deve ser um número inteiro."
    	exit 1
	fi

	# Calcula a nova data
	NEW_DATE=$(date -d "$1 days" +"%d-%m-%Y")

	# Imprime a nova data
	echo "Nova data: $NEW_DATE"
}

function duplicates() {
	[ ! $1 ] && DIR="$(pwd)" || DIR="$1"
	find "$DIR" ! -empty -type f -exec md5sum {} + | sort | uniq -w32 -dD
}

function large-files() {
	[ $1 ] && dir="$1" || dir="$(pwd)"
	[ $2 ] && num=$2 || num=10
    find $dir -printf '%s %p\n' 2> /dev/null | sort -nr | head -${num}
    #find $dir -type f -printf "%k : %p \n" 2> /dev/null | awk '{$1/=1024}1' | sort -nr | head -n $num
}

function show_colors() {
  # Cabeçalho da tabela
  echo "-----------------------------------"
  printf "%-20s | %-10s\n" "Comando" "Cor"
  echo "-----------------------------------"

  # Loop pelas cores básicas
  for i in {0..7}; do
    color=$(tput setaf $i)
    printf "${color}%-20s | %-10s${RESET}\n" "tput setaf $i" "Cor $i"
  done

  echo "-----------------------------------"
}

function mem-usage-color() {
  # Reset color
  RESET=$(tput sgr0)

  total=0

  # Cabeçalho da tabela
  printf "%-20s | %-10s\n" "Processo" "Memória (MB)"
  echo "----------------------------------"

  color_counter=1

  for arg in "$@"; do
    # Get the memory usage for the specific process
    mem=$(ps aux | grep -E "$arg" | grep -v "grep" | awk '{sum += $6} END {print sum/1024}')
    if [[ -z "$mem" ]]; then
      mem=0
    fi
    color=$(tput setaf $color_counter)
    printf "${color}%-20s | %-10s${RESET}\n" "$arg" "$mem"
    total=$(echo "$total + $mem" | bc)

    # Increment the color counter, and reset if it goes beyond 7 (for basic 8 colors)
    ((color_counter++))
    if [[ $color_counter -gt 7 ]]; then
      color_counter=1
    fi
  done
  
  echo "----------------------------------"
  printf "%-20s | %-10s\n" "Total" "$total"
}

function mem-usage() {
  # Cores usando tput
  RED=$(tput setaf 1)
  GREEN=$(tput setaf 2)
  CYAN=$(tput setaf 6)
  RESET=$(tput sgr0)

  total=0

  # Cabeçalho da tabela
  echo "${CYAN}----------------------------------${RESET}"
  printf "${CYAN}%-20s | %-10s${RESET}\n" "Processo" "Memória (MB)"
  echo "${CYAN}----------------------------------${RESET}"

  for arg in "$@"; do
    # Get the memory usage for the specific process
    mem=$(ps aux | grep -E "$arg" | grep -v "grep" | awk '{sum += $6} END {print sum/1024}')
    if [[ -z "$mem" ]]; then
      mem=0
    fi
    printf "${GREEN}%-20s | %-10s${RESET}\n" "$arg" "$mem"
    total=$(echo "$total + $mem" | bc)
  done
  
  echo "${CYAN}----------------------------------${RESET}"
  printf "${RED}%-20s | %-10s${RESET}\n" "Total" "$total"
}

function mem-usage-raw() {
  total=0
  echo "Comparação de Uso de Memória (MB):"

  for arg in "$@"; do
    # Get the memory usage for the specific process
    mem=$(ps aux | grep -E "$arg" | grep -v "grep" | awk '{sum += $6} END {print sum/1024}')
    if [[ -z "$mem" ]]; then
      mem=0
    fi
    echo "$arg: $mem MB"
    total=$(echo "$total + $mem" | bc)
  done
  
  echo "------------------------"
  echo "Total: $total MB"
}

function mem-usage-all() {
	result=""
	for arg in "$@"; do
		if [ -z "$result" ]; then
			result="$arg"
		else
			result="$result|$arg"
		fi
	done
  # MB
  ps aux | grep -E "$result" | awk '{sum += $6} END {print "Total de memória (MB):", sum/1024}'
  # KB
	# ps aux | grep -E "$result" | awk '{sum += $6} END {print "Total de memória (KB):", sum}'
}

function pdfbw() {
	[ ! $2 ] && return
	[ ! -f $1 ] || [ ! -f $2 ] && return
  gs -sOutputFile="$2" -sDEVICE=pdfwrite -sColorConversionStrategy=Gray -dProcessColorModel=/DeviceGray -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH "$1"
}

# system
function broken-links() {
    [ $1 ] && dir=$1 || dir=$(pwd)
    find "$dir" -type l ! -exec test -e {} \; -print
}

function appusage() {
    top -b -n 2 -d 0.2 -p $(pidof -s $1) | tail -1 | awk '{print $9}'
}

function list-fonts() {
    [ ! $1 ] && return
    fc-list | awk -F':' '{print $2}' | grep -i $1 | awk '{$1=$1};1' | sort | uniq
}

function cpr() {
    rsync --archive -hh --partial --info=stats1,progress2 --modify-window=1 "$@"
} 

function mvr() {
    rsync --archive -hh --partial --info=stats1,progress2 --modify-window=1 --remove-source-files "$@"
}

# irc
function sp() {
    [ ! $1 ] && return
    senpai -config ~/.config/senpai/$1.scfg
}

# docker
function dlg () {
    docker exec -it $1 bash
}

function dbl () {
    docker build -t $1 .
}

function dru () {
    docker run --name $1 --network host -itd $2
}

# mpv
function mm() {
    params=\"$@\"
    killall mpv 1> /dev/null 2> /dev/null
    sleep 1
    (mpv --really-quiet --profile=youtube-cache ytdl://ytsearch:"$params") > /dev/null 2> /dev/null &
}

function mma() {
    mpv --no-video --ytdl-format=bestaudio ytdl://ytsearch:"$@" # ytdl://ytsearch10:"$@"
}

# ssh
# function sshm() {
#     [ ! -d $HOME/sshfs ] && mkdir -p $HOME/sshfs
#     [ ! $1 ] && return

#     #fusermount3 -u $HOME/sshfs/${2}

# 	if [ "$1" == "-u" ] || [ "$1" == "--unmount" ]; then
# 		[ ! $2 ] && return
# 		[ -d $HOME/sshfs/$2 ] && fusermount3 -u $HOME/sshfs/${2}
# 	else
# 	    [ ! -d $HOME/sshfs/$1 ] && mkdir $HOME/sshfs/${1}
#         sshfs root@${1}:/ $HOME/sshfs/$1 -o follow_symlinks -o dir_cache=no -o compression=no
# 	fi
# }
function sshm() {
    [ ! $1 ] && return
	[ ! -d $HOME/sshfs/$1 ] && \mkdir -p $HOME/sshfs/${1}
    # sshfs -C root@${1}:/ $HOME/sshfs/$1 -o follow_symlinks -o dir_cache=no -o compression=no
    sshfs -C root@${1}:/ $HOME/sshfs/$1 -o follow_symlinks
}

function sshum() {
    [ ! $1 ] && return
    fusermount3 -u $HOME/sshfs/${1}
}

# rsync
function fullsync() {
    [ ! -d $STORAGE/vps/$1 ] && mkdir -p $STORAGE/vps/${1}
    
    rsync -aAXvzz \
    --exclude-from "$HOME/.config/rsync-excludes.list" \
    root@${1}:/ $STORAGE/vps/${1}/
}

function fullsite() {
    [ ! -d $STORAGE/sites/${1} ] && mkdir -p $STORAGE/sites/${1}
    rsync -aAXvzz \
        --exclude="node_modules/" \
        --exclude="*.mp4" \
        --exclude="*.mp3" \
        --exclude=".git/" \
        --exclude=".gitignore" \
        nginx@${1}:/var/www/ $STORAGE/sites/${1}/
}

function fullsitereverse() {
    if [ -d $STORAGE/sites/${1} ]; then
        rsync -aAXvzz \
            --exclude="vendor/" \
            --exclude="node_modules/" \
            --exclude="*.mp4" \
            --exclude="*.mp3" \
            --exclude=".git/" \
            --exclude=".gitignore" \
            $STORAGE/sites/${1}/var/www/${1}.radiochat.com.br nginx@${1}:/var/www/
    fi
}

function songdown() {
    [ ! -d $STORAGE/audio/${1} ] && mkdir -p $STORAGE/audio/${1}

    rsync -aAXvzz \
    liquidsoap@${1}:/opt/liquidsoap/music/ \
    $STORAGE/audio/${1}/ $2

    find $STORAGE/audio/${1} -type d -exec chmod 755 '{}' \; 
    find $STORAGE/audio/${1} -type f -exec chmod 644 '{}' \;
}

function songup() {
    [ $1 ] && maquina="$1" || exit
    shift

    #[ ! -d $STORAGE/audio/${1} ] && mkdir -p $HOME/audio/${1}   

    #if [ -d $STORAGE/audio/${1} ]; then 
    #    find $STORAGE/audio/${1} -type d -exec chmod 755 '{}' \; 
    #    find $STORAGE/audio/${1} -type f -exec chmod 644 '{}' \;
    #fi

    rsync -aAXvzz \
    $STORAGE/audio/sertanejo/ \
    liquidsoap@${maquina}:/media/musicas/ $2

    #ssh root@${1} "chown -R liquidsoap /opt/liquidsoap/music/"
}

#
# File Utils
#
# webp -> png
function webp2png() {
	[ ! $1 ] && return
	dwebp "$1" -o "${1:r}.png"
}

# iso
function checkiso() {
    if [ -f SHA512SUMS ]; then
        sha512sum --ignore-missing -c SHA512SUMS
        return
    fi
    
    if [ -f SHA256SUMS ]; then
        sha256sum --ignore-missing -c SHA256SUMS
        return
    fi
}

# mp3
function bitrate () {
    echo `basename "$1"`: `file "$1" | sed 's/.*, \(.*\)kbps.*/\1/' | tr -d " " ` kbps
}

# mp4
function ffmpeghalf () {
    local filename="${1%.*}"
    local timestamp=$(date +%s)
    ffmpeg -i "$1" -vf scale="iw/2:ih/2" "${filename}.${timestamp}.mp4"
}

function dresolve() {
    ffmpeg -i $1 -c:v dnxhd -profile:v dnxhr_hq -pix_fmt yuv422p -c:a pcm_s16le -f mov ${1%.*}.mov
}

function twitch() {
     INRES="1920x1080" # input resolution
     OUTRES="1920x1080" # output resolution
     FPS="15" # target FPS
     GOP="30" # i-frame interval, should be double of FPS, 
     GOPMIN="15" # min i-frame interval, should be equal to fps, 
     THREADS="2" # max 6
     CBR="1000k" # constant bitrate (should be between 1000k - 3000k)
     QUALITY="ultrafast"  # one of the many FFMPEG preset
     AUDIO_RATE="44100"
     STREAM_KEY=$(cat $HOME/.twitch) # use the terminal command Streaming streamkeyhere to stream your video to twitch or justin
     SERVER="live-sao" # twitch server in frankfurt, see http://bashtech.net/twitch/ingest.php for list
     
     ffmpeg -f x11grab -s "$INRES" -r "$FPS" -i :0.0 -f pulse -i 0 -f flv -ac 2 -ar $AUDIO_RATE \
       -vcodec libx264 -g $GOP -keyint_min $GOPMIN -b:v $CBR -minrate $CBR -maxrate $CBR -pix_fmt yuv420p\
       -s $OUTRES -preset $QUALITY -tune film -acodec libmp3lame -threads $THREADS -strict normal \
       -bufsize $CBR "rtmp://$SERVER.twitch.tv/app/$STREAM_KEY"
 }

# mpc
function mpcr () {
    if [ $1 ]; then
        mpc rm $1
        mpc save $1
        mpc clear
        mpc load $1
        mpc play
    fi
}

function mpcl () {
    $HOME/bin/mpc.sh
}

# gnome
function gnome-reset() {
    gsettings list-schemas | grep org.gnome | xargs -n 1 gsettings reset-recursively
}

function chshell() {
    if [ ! $1 ]; then
        echo "Temas disponíveis:"
        echo
        for tema in $(/usr/bin/ls /usr/share/themes); do
            if [ -d /usr/share/themes/${tema}/gnome-shell ]; then
            	echo $tema
            fi
        done
        if [ -d $HOME/.local/share/themes ]; then
            for tema in $(/usr/bin/ls $HOME/.local/share/themes); do
                if [ -d $HOME/.local/share/themes/${tema}/gnome-shell ]; then
                    echo $tema
                fi
            done
        fi
        return
    fi

    if [ ! -d $HOME/.local/share/themes/${1}/gnome-shell ]; then
        if [ ! -d /usr/share/themes/${1}/gnome-shell ]; then
            echo "Tema inválido"
            return
        fi
    fi
    echo "Trocando o tema"
    gsettings set org.gnome.shell.extensions.user-theme name "$1"
}

function chgtk() {
    if [ ! $1 ]; then
        echo "Temas disponíveis:"
        for tema in $(/usr/bin/ls /usr/share/themes); do
            if [ -d /usr/share/themes/${tema} ]; then
                if [ -d /usr/share/themes/${tema}/gtk-3.0 ] || [ -d /usr/share/themes/${tema}/gtk-4.0 ]; then
                    echo $tema
                fi
            fi
        done
        if [ -d $HOME/.local/share/themes ]; then 
            for tema in $(/usr/bin/ls $HOME/.local/share/themes); do
                if [ -d $HOME/.local/share/themes/${tema} ]; then
                    if [ -d $HOME/.local/share/themes/${tema}/gtk-3.0 ]; then
                        echo $tema
                    fi
                fi
            done
        fi
        return
    fi

    if [ ! -d $HOME/.local/share/themes/$1/gtk-3.0 ]; then
        if [ ! -d /usr/share/themes/$1/gtk-3.0 ]; then
            echo "Tema inválido"
            return
        fi
    fi
    
    echo "Trocando o tema"    
	
    if [ "$DESKTOP_SESSION" = "gnome" ]; then
		    echo "Trocando o tema no ambiante Gnome..."
        gsettings set org.gnome.desktop.interface gtk-theme "$1"
    fi
 
    #[ -f $HOME/.config/gtk-4.0/settings.ini ] && sed "/^gtk-theme-name =/s/=.*/= $1/" $HOME/.config/gtk-4.0/settings.ini
    [ -f $HOME/.config/gtk-3.0/settings.ini ] && sed -i "/^gtk-theme-name[[:space:]]*=/s/=.*/= $1/" $HOME/.config/gtk-3.0/settings.ini
    [ -f $HOME/.config/gtk-4.0/settings.ini ] && sed -i "/^gtk-theme-name[[:space:]]*=/s/=.*/= $1/" $HOME/.config/gtk-4.0/settings.ini
}

function chicon() {
    if [ ! $1 ]; then
        echo "Temas disponíveis:"
        for tema in $(/usr/bin/ls /usr/share/icons); do
            if [ -d "/usr/share/icons/${tema}" ]; then
                echo $tema
            fi            
        done
        for tema in $(/usr/bin/ls $HOME/.local/share/icons 2> /dev/null); do
            if [ -d "$HOME/.local/share/icons/${tema}" ]; then
                echo $tema
            fi
        done
        return
    fi

    if [ ! -d $HOME/.local/share/icons/$1 ]; then
        if [ ! -d /usr/share/icons/$1 ]; then
            echo "Tema inválido"
            return
        fi
    fi
    
    echo "Trocando o tema" 

    if [ "$DESKTOP_SESSION" = "gnome" ]; then
        echo "Trocando o tema no ambiante Gnome..."
        gsettings set org.gnome.desktop.interface icon-theme "$1"
    fi

    [ -f $HOME/.config/gtk-3.0/settings.ini ] && sed -i "/^gtk-icon-theme-name[[:space:]]*=/s/=.*/= $1/" $HOME/.config/gtk-3.0/settings.ini
    [ -f $HOME/.config/gtk-4.0/settings.ini ] && sed -i "/^gtk-icon-theme-name[[:space:]]*=/s/=.*/= $1/" $HOME/.config/gtk-4.0/settings.ini
    #[ -f $HOME/.config/gtk-4.0/settings.ini ] && sed "/^gtk-icon-theme-name =/s/=.*/= $1/" $HOME/.config/gtk-4.0/settings.ini
}

function rsticon() {
    gsettings reset org.gnome.desktop.interface icon-theme
}

function rstshell() {
    gsettings reset org.gnome.shell.extensions.user-theme name
}

function rstgtk() {
    gsettings reset org.gnome.desktop.interface gtk-theme
}

function rstcursor() {
    gsettings reset org.gnome.desktop.interface cursor-theme
}

function dos2unix() {
    sed -i 's/^M$//' "$1"     # DOS to Unix
    #sed 's/$/^M/'            # Unix to DOS
    sed -i $'s/\r$//' "$1"    # DOS to Unix
    #sed $'s/$/\r/'           # Unix to DOS
}

# git
# https://stackoverflow.com/a/1968538/1844007
function compare-remotes() {
    local REPO="$(basename $(pwd))"
    local REMOTE
    local REMOTE_BRANCH
    local LOCAL_BRANCH

    vared -p 'Endereço do repositorio: ' -c REMOTE
    vared -p 'Branch do repositorio remoto: ' -c REMOTE_BRANCH
    vared -p 'Branch do repositorio local: ' -c LOCAL_BRANCH

    git remote add -f upstream $REMOTE 2> /dev/null
    git remote update
    git diff $LOCAL_BRANCH remotes/upstream/$REMOTE_BRANCH >> ${HOME}/${REPO}.diff
    git remote rm upstream
}

function set-upstream() {
    [ ! $1 ] && return
    git remote set-url origin $1
}

function rename-branch() {
		#git branch -m <old_name> <new_name>

		# Delete the old branch on remote - where <remote> is, for example, origin
		#git push <remote> --delete <old_name>

		# Or shorter way to delete remote branch [:]
		#git push <remote> :<old_name>

		# Prevent git from using the old name when pushing in the next step.
		# Otherwise, git will use the old upstream name instead of <new_name>.
		#git branch --unset-upstream <new_name>

		# Push the new branch to remote
		#git push <remote> <new_name>

		# Reset the upstream branch for the new_name local branch
		#git push <remote> -u <new_name>

    git checkout $1
    git branch -m $2
    git push origin -u $2
    git push origin --delete $1
}

function delete-branch() {
    old=$(git rev-parse --abbrev-ref HEAD)
    git checkout -
    git branch -D $old
    git push origin --delete $old
}

function new-empty-branch() {
  [ ! $1 ] && return
  [ ! -d .git ] && return
	[ "$(git branch --list $1)" ] && return
	git checkout --orphan $1
	git rm --cached -r .
	#find /directory/ -type f -not -name 'PATTERN' -delete
}

function new-branch() {
    git checkout -b $1
}

function create-diff() {
    git diff $1 > ../patch.diff
}  

function apply-patch() {
    git checkout $1
    git apply $2
}

function first-commit() {
    [ -d .git ] && return

    local EMPTY=1
    local PROJECT="$(basename "$(pwd)")"
    local PARENT="$(dirname "$(pwd)")"
    local MESSAGE="First commit 🥳"

    echo "Repo name: $PROJECT"
    echo "Parent dir: $PARENT"

    [ $1 ] && MESSAGE="$@"

    [ "$(\ls -A .)" ] && EMPTY=0
   
    git init
    
    [ ! -f README.md ] && echo "# $PROJECT" >> README.md
    [ $EMPTY -eq 1 ] && git add README.md || git add .

    git commit -m "$MESSAGE"
    git branch -M main
    git remote add origin git@github.com:sistematico/${PROJECT}.git
    git push -u origin main
}

function remove-commit() {
    FILTER_BRANCH_SQUELCH_WARNING=1 ; \
    git filter-branch --force --index-filter \                                                                                                    
        "git rm --cached --ignore-unmatch $1" \
        --prune-empty --tag-name-filter cat -- --all
}

function commit() {
  icon=":robot_face:"
  upstream="$(git config --get remote.origin.url)"
  test "${string#*"github"}" != "$upstream" && icon="🤖"

    #[ -f .commit ] && msg="$(cat .commit)" || msg="Commit automático"
    [ -f .commit ] && msg="$icon $(cat .commit)" || msg="$icon Alterações: $(git status -s -z)"
    [ $1 ] && msg="$@"
    git add .
    git commit -m "$msg"
    git push
}

function remove-last-commit() {
    git push -f origin HEAD^:main
}

function autocommit() {
    if [ -d .git ]; then
        curl -s -L https://git.io/JzKB2 -o .git/hooks/post-commit
        chmod +x .git/hooks/post-commit
        git config --local commit.template .commit

        if [ ! -f .commit ] || [ ! -s .commit ]; then
            echo "Update automático" > .commit
        fi

        if [ ! -f .gitignore ] || [ ! -s .gitignore ]; then
            echo ".commit" > .gitignore
        else
            if ! grep -Fxq ".commit" .gitignore 2> /dev/null; then
                echo ".commit" >> .gitignore        
            fi
        fi
    fi
}

function create-patch() {
    git format-patch --stdout HEAD^ > $(basename $(pwd))-$(git log -1 --pretty=format:%B | awk '{print tolower($1)}')-$(date '+%Y%m%d')-$(git rev-parse --short HEAD).diff
}

# images
function heic() {
    for i in *.heic; do heif-convert "$i" "${i%.heic}.jpg"; done
}

# suckless
function rdwm() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/dwm-flexipatch
  
  $EDITOR -p config.def.h patches.def.h
  
  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h patches.hi
    make
    sudo make install clean
  fi

  if ! cmp patches.def.h patches.h >/dev/null 2>&1; then
    rm -f config.h patches.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rdmenu() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/dmenu

  $EDITOR config.def.h

  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rst() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/st

  $EDITOR config.def.h

  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rslock() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/slock

  $EDITOR config.def.h

  if ! cmp config.def.h config.h >/dev/null 2>&1; then
    rm -f config.h
    make
    sudo make install clean
  fi

  cd "$OLDPWD"
}

function rdwmblocks() {
  OLDPWD="$(pwd)"
  cd $HOME/gitlab/dwmblocks

  $EDITOR blocks.def.h

  if ! cmp blocks.def.h blocks.h >/dev/null 2>&1; then
    rm -f blocks.h
    make
    sudo make install clean
		pkill -USR1 dwmblocks
  fi

  cd "$OLDPWD"
}

# tmux
function tun() {
  [ ! $1 ] || [ ! -f ~/.tmux/config/$1.conf ] && return

  if ! tmux has-session -t $1 2> /dev/null; then
    tmux new-session -s $1 -n home -d 
    tmux source-file ~/.tmux/config/${1}.conf
  else
    tmux attach -t $1
  fi
}

