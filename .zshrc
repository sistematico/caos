# Console ScreenSaver
#TMOUT=300
#TRAPALRM() { cmatrix -s -a -b -C blue -M 'lucas@caos' }

# EnvVars
export EDITOR="vim"

# PATH
export PATH=$PATH:$HOME/.local/bin # ~/.local/bin
export PATH=$PATH:$HOME/.bun/bin   # bun
export PATH=$PATH:$HOME/.deno/bin  # deno

# Autoloads
autoload -U select-word-style
select-word-style bash

autoload -Uz compinit
compinit

# Configurations
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd

#zstyle :compinstall filename '/home/lucas/.zshrc'

[ -f $HOME/.zsh_keybindings ] && source $HOME/.zsh_keybindings
[ -f $HOME/.zsh_aliases ] && source $HOME/.zsh_aliases
[ -f $HOME/.zsh_functions ] && source $HOME/.zsh_functions

[ -s /usr/share/doc/pkgfile/command-not-found.zsh ] && source /usr/share/doc/pkgfile/command-not-found.zsh
fpath=(~/.zsh.d/ $fpath)

eval "$(starship init zsh)"

# bun completions
[ -s "/home/lucas/.bun/_bun" ] && source "/home/lucas/.bun/_bun"

#fortune chucknorris
#echo

# pnpm
export PNPM_HOME="/home/lucas/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

source /home/lucas/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Catppuccin
#source ~/.zsh/catppuccin_mocha-zsh-syntax-highlighting.zsh
#source ~/.zsh/catppuccin_frappe-zsh-syntax-highlighting.zsh
#source ~/.zsh/catppuccin_latte-zsh-syntax-highlighting.zsh
#source ~/.zsh/catppuccin_macchiato-zsh-syntax-highlighting.zsh

#[ -f $HOME/.zsh/lscolors.zsh ] && source $HOME/.zsh/lscolors.zsh
[ -f /etc/profile.d/vte.sh ] && source /etc/profile.d/vte.sh
