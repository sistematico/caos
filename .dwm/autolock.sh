#!/usr/bin/env bash

#LOCK=${LOCK:-$(xset q | awk '/timeout:/{print $2}')}
#SUSPEND=${SUSPEND:-$(xset q | awk '/Standby:/{print $4}')}

PARAMS="--not-when-audio" # --not-when-fullscreen
LOCK=360     # Seconds
SUSPEND=3000 # Seconds
CSUSPEND=$(( SUSPEND - LOCK ))

xidlehook --not-when-audio --timer $LOCK 'slock' '' --timer $CSUSPEND 'systemctl suspend' ''
