#!/usr/bin/env bash

wp=

if [ -f $HOME/.dwm/wp.txt ]; then
    readarray -t wp < $HOME/.dwm/wp.txt
else
    wp=(
      "/home/lucas/img/wallpapers/paxa/paxa-1.png"
      "/home/lucas/img/everforest/close_up/circuit_1.png" 
      "/home/lucas/img/everforest/close_up/circuit_2.png" 
      "/home/lucas/img/everforest/close_up/rocks_2.png" 
      "/home/lucas/img/everforest/close_up/rocks_2.png" 
      "/home/lucas/img/everforest/close_up/waves_1.png"
      "/home/lucas/img/everforest/close_up/waves_2.png"
    )
fi

size=${#wp[@]}
index=$(($RANDOM % $size))

feh --bg-fill $(echo ${wp[$index]})
